#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Main program file.
"""

import os
import sys
from kivy import resources
from kivy.app import App
from kivy.uix.settings import SettingsWithSidebar
from kivy.config import Config, ConfigParser
from kivy.clock import Clock
from tinytag import TinyTag
from configs.settingsjson import media_settings_json, obd2_settings_json
from lib.audio.player import import_player
from lib.constants import PROTOCOLS_MAP, WRONG_CHARS
from lib.obd2.obd2 import OBD2
from lib.uix.songlayout import SongLayout, SongLabel


os.environ['KIVY_HOME'] = os.getcwd()
Config.set('graphics', 'width', '1024')
Config.set('graphics', 'height', '600')
Config.write()


class CarApp(App):
    """
    Main application class.
    Draws the interface and creates instances of Player and OBD2 listener with graphs.
    """
    cp = ConfigParser()
    player = None
    obd2 = None
    need_obd = False

    def __init__(self, **kwargs):
        super().__init__()

    def build(self):
        """
        Function is called when CarApp object is created.
        :return:
        """
        self.settings_cls = SettingsWithSidebar
        self.use_kivy_settings = True
        self.init_player()
        self.init_obd2()
        volume = self.player.get_volume()
        self.root.ids.volumeSlider.value = volume
        self.root.ids.volumeSlider.bind(value=self.player.set_volume)
        Clock.schedule_interval(self.update, 0.3)

    def update(self, _):
        """
        Function used as a callback for timer.
        :param args:
        :return:
        """
        try:
            if self.player:
                self.player.update()
                self.root.ids.songSlider.max = len(self.player.song.chunks)
                self.root.ids.songSlider.value = self.player.song.chunk
        except Exception as exceptn:
            print("Exception in update: " + str(exceptn))

    def init_player(self):
        """
        Initialise the music player.
        :return:
        """
        self.player = None
        Player = import_player()
        extensions = list()
        playlist = {}
        fpath = self.get_running_app().config.get('Media', 'path')
        if self.get_running_app().config.get('Media', 'mp3'):
            extensions.append('mp3')
        if self.get_running_app().config.get('Media', 'flac'):
            extensions.append('flac')
        if self.get_running_app().config.get('Media', 'm4a'):
            extensions.append('m4a')
        i = 1
        for root, _, files in os.walk(fpath):
            for file in files:
                for ext in extensions:
                    if file.endswith(ext):
                        fullpath = str(os.path.join(root, file))
                        playlist[i] = {'path': fullpath}
                        i += 1
        self.player = Player(current_song=1,
                             playlist=playlist,
                             parent=self)
        self.view_playlist()
        print('Music player initialized.')

    def init_obd2(self):
        """
        Initialise the OBD2 listener and graphs.
        :return:
        """
        need_obd = self.get_running_app().config.get('OBD2', 'enable')
        if bool(need_obd):
            self.obd2 = None
            obd_port = self.get_running_app().config.get('OBD2', 'port')
            obd_baud = self.get_running_app().config.get('OBD2', 'baud')
            obd_protocol = PROTOCOLS_MAP[self.get_running_app().config.get('OBD2', 'protocol')]
            vef = self.get_running_app().config.get('OBD2', 'vef')
            eds = self.get_running_app().config.get('OBD2', 'eds')
            my_time = self.get_running_app().config.get('OBD2', 'time')
            self.obd2 = OBD2(port=obd_port,
                             baud=obd_baud,
                             protocol=obd_protocol,
                             vef=vef,
                             eds=eds,
                             time=my_time,
                             parent=self)
            try:
                for pid in self.obd2.pids:
                    self.root.ids.graphScreen.add_widget(self.obd2.pids[pid]['gauge'].graph)
                print('OBD2 initialized')
            except Exception as exceptn:
                print("Exception occured: " + str(exceptn))

    def list_song(self, song_id):
        """
        Adds song to playlist (UI).
        :param id:
        :return:
        """
        exists = False
        is_current = song_id == self.player.current_song
        for kid in self.root.ids.songlist.children:
            if isinstance(kid, SongLayout):
                if kid.id == str(song_id):
                    exists = True
                    if is_current:
                        kid.current = True
                        for grandkid in kid.children:
                            grandkid.color = [1, 1, 1, 1]
                    else:
                        kid.current = False
                        for grandkid in kid.children:
                            grandkid.color = [0.5, 0.5, 0.5, 1]
        if not exists:
            song_layout = SongLayout(id=song_id, current=False)
            tag = TinyTag.get(self.player.playlist[song_id]['path'])
            if (bool(set(WRONG_CHARS) & set(str(tag.title)))):
                tag.title = tag.title.encode('latin1').decode('cp1251')
            if (bool(set(WRONG_CHARS) & set(str(tag.album)))):
                tag.album = tag.album.encode('latin1').decode('cp1251')
            if (bool(set(WRONG_CHARS) & set(str(tag.artist)))):
                tag.artist = tag.artist.encode('latin1').decode('cp1251')
            line = '{:>4}  {:30.30}  {:25.25}  {:25.25}  {:10.10}'.format(str(song_id),
                                                                          str(tag.title),
                                                                          str(tag.album),
                                                                          str(tag.artist),
                                                                          str(tag.genre))
            lid = "Label_" + str(song_id)
            label = SongLabel(id=lid, text=line, active=is_current, root=self)
            song_layout.add_widget(label)
            self.root.ids.songlist.add_widget(song_layout)

    def set_current(self, song_id):
        """
        Switches the current song to one with identity number song_id.
        :param song_id:
        :return:
        """
        self.player.current_song = int(song_id)
        self.view_playlist()

    def view_playlist(self):
        """
        View playlist (UI).
        :return:
        """
        self.player.stop()
        self.root.ids.songlist.clear_widgets()
        for song_id in self.player.playlist:
            self.list_song(song_id)

    def update_volume(self, volume):
        """
        Sets the volume according to volume slider.
        :param volume:
        :return:
        """
        self.player.set_volume(volume)

    def build_config(self, config):
        """
        Creates config if not found. Initialise the default config values.
        :param config:
        :return:
        """
        config.setdefaults('Media', {
            'path': '/Users/shark/Downloads/Russian Metall Balads',
            'shuffle': 1,
            'mp3': 1,
            'm4a': 1,
            'flac': 1
        })
        config.setdefaults('OBD2', {
            'enable': 1,
            'port': '/dev/ttys005',
            'baud': 38400,
            'protocol': 'AUTO, ISO 9141-2',
            'vef': 87.68,
            'eds': 1.4,
            'time': 0.3
        })

    def build_settings(self, settings):
        """
        Adding config settings from JSON.
        :param settings:
        :return:
        """
        settings.add_json_panel('Media', self.config, data=media_settings_json)
        settings.add_json_panel('OBD2', self.config, data=obd2_settings_json)

    def on_config_change(self, config, section, key, value):
        """
        Callback, executed when configuration is changed.
        :param config:
        :param section:
        :param key:
        :param value:
        :return:
        """
        print("Config changed! Section " + str(section))
        self.cp.write()
        if section == "Media":
            self.init_player()
        if section == "OBD2":
            self.init_obd2()


def resourcePath():
    '''Returns path containing content - either locally or in pyinstaller tmp file'''
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS)

    return os.path.join(os.path.abspath("."))


if __name__ == '__main__':
    resources.resource_add_path(resourcePath())  # add this line
    CarApp().run()
