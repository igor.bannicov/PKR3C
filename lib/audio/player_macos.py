# -*- coding: utf-8 -*-
"""
Player implementation for OSX
"""
import platform
SYSTEM = platform.system()
if SYSTEM == 'Darwin':
    import osascript
elif SYSTEM == 'Linux':
    import alsaaudio
from threading import Thread, Event
from pyaudio import PyAudio
from lib.audio.song import Song


class Player(Thread):
    """
    Player class
    """

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.playlist = kwargs['playlist']
        self.current_song = kwargs['current_song']
        self.parent = kwargs['parent']
        self.song = Song(self.playlist[self.current_song]['path'])
        self.is_paused = True
        self.engine = PyAudio()
        self.daemon = True
        if SYSTEM == 'Darwin':
            self.mixer = None
        elif SYSTEM == 'Linux':
            mixers = alsaaudio.mixers(cardindex=0)
            self.mixer = alsaaudio.Mixer(mixers[0])
        self.start()
        self._stop_event = Event()

    def __get_stream(self):
        """
        Returns current audio stream
        :return:
        """
        return self.engine.open(
            format=self.engine.get_format_from_width(self.song.audio.sample_width),
            channels=2,
            rate=self.song.audio.frame_rate,
            output=True)

    def pause(self):
        """
        Pause player
        :return:
        """
        self.is_paused = True

    def play(self):
        """
        Start playing
        :return:
        """
        self.is_paused = False

    def stop(self):
        """
        Stops player
        :return:
        """
        self.is_paused = True
        self.song = Song(self.playlist[self.current_song]['path'])

    def previous(self):
        """
        Switch to previous song
        :return:
        """
        if self.current_song > 0:
            self.current_song = self.current_song - 1
        else:
            self.current_song = len(self.playlist) - 1
        self.parent.set_current(self.current_song)
        if not self.is_paused:
            self.stop()
            self.play()
        else:
            self.stop()

    def next(self):
        """
        Switch to next song
        :return:
        """
        if self.current_song < (len(self.playlist) - 1):
            self.current_song = self.current_song + 1
        else:
            self.current_song = 0
        self.parent.set_current(self.current_song)
        if not self.is_paused:
            self.stop()
            self.play()
        else:
            self.stop()

    def change(self, song_id):
        """
        Switch to song song_id
        :param song_id:
        :return:
        """
        self.current_song = song_id
        self.parent.set_current(song_id)
        if not self.is_paused:
            self.stop()
            self.play()
        else:
            self.stop()

    def get_volume(self):
        """
        !!! TBD !!!
        Returns the player current audio volume
        :return:
        """
        if SYSTEM == 'Darwin':
            volume = osascript.osascript("output volume of (get volume settings)")
            return int(volume[1])
        elif SYSTEM == 'Linux':
            volume = self.mixer.getvolume()
            return int(volume[0])


    def set_volume(self, _, value):
        """
        !!! TBD !!!
        Sets the player audio volume
        :param value:
        :return:
        """
        my_volume = int(value)
        if SYSTEM == 'Darwin':
            if my_volume != self.get_volume():
                osascript.osascript("set volume output volume " + str(my_volume))
        elif SYSTEM == 'Linux':
            self.mixer.setvolume(int(value))

    def run(self):
        """
        Main class method
        :return:
        """
        stream = self.__get_stream()
        while self.song.chunk <= len(self.song.chunks):
            if not self.is_paused:
                try:
                    data = (self.song.chunks[self.song.chunk])._data
                    self.song.chunk += 1
                    stream.write(data)
                except:
                    self.next()
        stream.stop_stream()
        self.engine.terminate()

    def update(self):
        """

        :return:
        """
        if not self.song:
            self.song = Song(self.playlist[self.current_song]['path'])
