# -*- coding: utf-8 -*-
"""
Import player dependant on OS platform.
Currently OSX and Linux only are supported.
"""
import platform

SYSTEM = platform.system()


def import_player():
    """
    Import player dependant on OS platform.
    :return:
    """
    if SYSTEM == 'Darwin':
        from lib.audio.player_macos import Player
    elif SYSTEM == 'Linux':
        from lib.audio.player_linux import Player
    return Player
