# -*- coding: utf-8 -*-
"""
Player implementation for Linux.
ALSA - based.
"""

from threading import Thread, Event
import alsaaudio
from lib.audio.song import Song


class Player(Thread):
    """
    Player class
    """

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.playlist = kwargs['playlist']
        self.current_song = kwargs['current_song']
        self.parent = kwargs['parent']
        self.song = None
        self.is_paused = True
        self.is_set = False
        self.engine = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, device='default')
        self.mixer = self.get_mixer()
        self.daemon = True
        self.start()
        self._stop_event = Event()

    def get_mixer(self):
        """
        Get available mixer for current setup
        :return:
        """
        try:
            mixers = alsaaudio.mixers(cardindex=0)
            print(mixers)
            if 'PCM' in mixers:
                mixer = alsaaudio.Mixer(mixers[0])
        except Exception as exceptn:
            print("Could not init mixer due to error: " + str(exceptn))
        return mixer

    def setup_audio(self):
        """
        Setup audio output
        :return:
        """
        self.song = Song(self.playlist[self.current_song]['path'])
        self.engine.setchannels(self.song.audio.channels)
        self.engine.setrate(self.song.audio.frame_rate)
        self.engine.setformat(alsaaudio.PCM_FORMAT_S16_LE)
        self.engine.setperiodsize(int(self.song.splitter / self.song.audio.channels))
        self.is_set = True

    def pause(self):
        """
        Pause playback
        :return:
        """
        print("Pausing playback.")
        self.is_paused = True

    def play(self):
        """
        Start playback
        :return:
        """
        print("Starting playback.")
        if not self.is_set:
            self.setup_audio()
        self.is_paused = False

    def stop(self):
        """
        Stop playback
        :return:
        """
        print("Stopping playback.")
        self.is_paused = True
        self.song = Song(self.playlist[self.current_song]['path'])
        self.engine.setrate(self.song.audio.frame_rate)
        self.engine.setperiodsize(int(self.song.splitter / self.song.audio.channels))

    def get_volume(self):
        """
        Get current audio volume
        :return:
        """
        volume = self.mixer.getvolume()
        print(volume)
        return volume[0]


    def set_volume(self, _, value):
        """
        Set audio volume
        :param value:
        :return:
        """
        self.mixer.setvolume(int(value))

    def previous(self):
        """
        Switch to previous song in playlist
        :return:
        """
        print("Going to previous song")
        if self.current_song > 0:
            self.current_song = self.current_song - 1
        else:
            self.current_song = len(self.playlist) - 1
        self.parent.set_current(self.current_song)
        self.setup_audio()
        if not self.is_paused:
            self.stop()
            self.play()
        else:
            self.stop()

    def next(self):
        """
        Switch to next song in playlist
        :return:
        """
        print("Going to next song")
        if self.current_song < (len(self.playlist) - 1):
            self.current_song = self.current_song + 1
        else:
            self.current_song = 0
        self.parent.set_current(self.current_song)
        self.setup_audio()
        if not self.is_paused:
            self.stop()
            self.play()
        else:
            self.stop()

    def change(self, song_id):
        """
        Set song with id song_id as current
        :param song_id:
        :return:
        """
        print("Going to song " + str(song_id))
        self.current_song = song_id
        self.parent.set_current(song_id)
        self.setup_audio()
        if not self.is_paused:
            self.stop()
            self.play()
        else:
            self.stop()

    def run(self):
        """
        Main class method
        :return:
        """
        self.setup_audio()
        while self.song.chunk <= len(self.song.chunks):
            if not self.is_paused:
                try:
                    data = (self.song.chunks[self.song.chunk])._data
                    self.song.chunk += 1
                    self.engine.write(data)
                except:
                    self.next()

    def update(self):
        """

        :return:
        """
        if not self.song:
            self.song = Song(self.playlist[self.current_song]['path'])
