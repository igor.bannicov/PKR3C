# -*- coding: utf-8 -*-
"""
The Song class file
"""

from pydub import AudioSegment
from pydub.utils import make_chunks


class Song():
    """
    Song object class.
    """
    def __init__(self, f):
        self.path = f
        self.splitter = 500
        self.audio = AudioSegment.from_file(f)
        self.chunk = 0
        self.chunks = make_chunks(self.audio, self.splitter)
