
# -*- coding: utf-8 -*-

import pprint
from collections import OrderedDict

p = pprint.PrettyPrinter(indent=4)


def celsius_to_kelvin(cgrad):
    if cgrad != "None":
        kgrad = int(cgrad) + 273.15
    else:
        kgrad = 0
    return kgrad


def kelvin_to_celsius(kgrad):
    if kgrad != "None":
        cgrad = int(kgrad) - 273.15
    else:
        cgrad = 0
    return cgrad


def round_by(x, base=5):
    return int(base * round(float(x)/base))


class MultiOrderedDict(OrderedDict):
    def __setitem__(self, key, value):
        if isinstance(value, list) and key in self:
            self[key].extend(value)
        else:
            super().__setitem__(key, value)
