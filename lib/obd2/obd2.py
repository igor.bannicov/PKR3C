# -*- coding: utf-8 -*-
"""
OBD2 Class file
"""
import os
from threading import Thread, Event
from time import sleep
import obd
from obd import OBDStatus
from configobj import ConfigObj
from lib.utils import celsius_to_kelvin
from lib.obd2.gauges import GaugeLabel, GaugeGraph
obd.logger.removeHandler(obd.console_handler)


class OBD2(Thread):
    """
    OBD2 Class
    """

    EXT = 0

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.port = kwargs['port']
        self.baud = kwargs['baud']
        self.protocol = kwargs['protocol']
        self.vef = kwargs['vef']
        self.eds = kwargs['eds']
        self.parent = kwargs['parent']
        self.path = os.getcwd() + "/configs/pids.ini"
        if os.path.exists(self.path):
            self.config = ConfigObj(self.path)
        else:
            print("PIDS file does not exist.")
            os._exit(9999)
        self.plottime = float(kwargs['time'])
        self.connection = None
        self.mai = 28.97    # Молекуляная масса воздуха
        self.rgas = 8.314      # Газовая постоянная
        self.d_gasoline = 750      # Плотность бензина (АИ-95)
        self.daemon = True
        self.pids = {}
        mypids = list(self.config['PIDS'].sections)
        for pid in mypids:
            gtype = self.config['PIDS'][pid]['type']
            if gtype == 'graph':
                try:
                    color = list(self.config['PIDS'][pid]['color'])
                except:
                    color = [1, 1, 1, 1]
                gauge = GaugeGraph(name=self.config['PIDS'][pid]['name'],
                                   color=color,
                                   grid=True,
                                   gtype=gtype,
                                   parent=self)
                self.pids[pid] = {'values': [],
                                  'name': self.config['PIDS'][pid]['name'],
                                  'gauge': gauge}
            elif gtype == 'label':
                try:
                    color = list(self.config['PIDS'][pid]['color'])
                except:
                    color = [1, 1, 1, 1]
                gauge = GaugeLabel(name=self.config['PIDS'][pid]['name'],
                                   color=color,
                                   gtype=gtype,
                                   parent=self)
                self.pids[pid] = {'value': 0,
                                  'name': self.config['PIDS'][pid]['name'],
                                  'gauge': gauge}

        self.start()
        self._stop_event = Event()

    def run(self):
        """
        Main class function
        :return:
        """
        while True:
            if self.connection:
                if not self.connection.status() == OBDStatus.CAR_CONNECTED:
                    self.connection = obd.OBD(self.port, self.baud, self.protocol)
                elif self.connection.status() == OBDStatus.CAR_CONNECTED:
                    self.get_all_pids()
            else:
                self.connection = obd.OBD(self.port, self.baud, self.protocol)

    def get_pid(self, pid):
        """
        Get data for specific PID
        :param pid:
        :return:
        """
        if self.connection.status() == OBDStatus.CAR_CONNECTED:
            cmd = obd.commands[pid]
            response = self.connection.query(cmd)
            if response == "":
                return None
            else:
                res = response.value.magnitude
            return res
        else:
            print("Not connected to OBD2")
            return None

    def get_all_pids(self):
        """
        Get data from all pids from internal list
        :return:
        """
        for pid in self.pids.keys():
            val = self.get_pid(pid)
            gtype = self.pids[pid]['gauge'].gtype
            if gtype == 'graph':
                values_list = self.pids[pid]['values']
                values_list.append(val)
                self.pids[pid]['values'] = values_list[-49:]
            if gtype == 'label':
                self.pids[pid]['value'] = val
            self.pids[pid]['gauge'].update()
            sleep(self.plottime)
        if self.EXT < 100:
            self.EXT += 1
            print(self.EXT)
        else:
            print("Exiting")
            os.system('kill %d' % os.getpid())


    def fuel_consumption(self):
        """
        Calculates fuel consumption based on MAP sensor
        :return:
        """
        rpm = int(self.get_pid("RPM"))
        intake_temp = celsius_to_kelvin(self.get_pid("INTAKE_TEMP"))
        intake_pressure = self.get_pid("INTAKE_PRESSURE")
        speed = self.get_pid("SPEED")
        if speed > 0:
            if (rpm != -999) and (intake_temp != -999) and (intake_pressure != -999):
                vfl = ((((((rpm * intake_pressure / (intake_temp * 2)) / 60) * (self.vef / 100) *
                          self.eds * self.mai / self.rgas) / 14.7) /
                        self.d_gasoline) * 3600) * 100 / speed
            else:
                vfl = 0
            units = "LPK"
        else:
            if (rpm != -999) and (intake_temp != -999) and (intake_pressure != -999):
                vfl = (((((rpm * intake_pressure / (intake_temp * 2)) / 60) *
                         (self.vef / 100) * self.eds * self.mai / self.rgas) / 14.7) /
                       self.d_gasoline) * 3600
            else:
                vfl = 0
            units = "LPH"
        return (vfl, units)

    def get_available_pids(self):
        """
        Get a list of available PIDS
        :return:
        """
        for command in self.connection.supported_commands:
            try:
                response = self.connection.query(command)
                print("PID : " + str(command) + " ->  " + str(response.value))
                sleep(self.plottime)
            except:
                print("PID : " + str(command) + "said OOPS!")

    def get_errors(self):
        """
        Get the current errors list
        :return:
        """
        try:
            cmd = obd.commands["GET_DTC"]
            response = self.connection.query(cmd)
            if response == "":
                res = "None"
            else:
                res = response.value
            return res
        except Exception as exceptn:
            return exceptn

    def clear_errors(self):
        """
        Clear errors
        :return:
        """
        try:
            cmd = obd.commands["CLEAR_DTC"]
            self.connection.query(cmd)
            return None
        except Exception as exceptn:
            return exceptn
