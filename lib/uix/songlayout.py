# -*- coding: utf-8 -*-

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.graphics import Rectangle, Color
from kivy.properties import BooleanProperty


class SongLayout(BoxLayout):

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.current = BooleanProperty(kwargs['current'])
        self.id = str(kwargs['id'])
        with self.canvas.before :
            self.color = Color(1, 0, 0, 1)
            self.rect = Rectangle(pos=self.pos, size=self.pos)


class SongLabel(Label):

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.halign = 'left'
        self.valign = 'middle'
        self.font_size = 16
        self.font_name = 'fonts/RobotoMono'
        self.id = kwargs['id']
        self.text = kwargs['text']
        self.root = kwargs['root']
        self.active = kwargs['active']
        self.background_color = (0.9, 0.9, 1, 1)
        self.bold = BooleanProperty(self.active)
        if self.active:
            self.color = (1, 1, 1, 1)
        else:
            self.color = (0.5, 0.5, 0.5, 1)

    def collides(self, pos):
        x2 = int(self.width / 2)
        y2 = 25
        if pos[0] in range(int(self.center_x) - x2, int(self.center_x) + x2):
            if pos[1] in range(int(self.center_y) - y2, int(self.center_y) + y2):
                return True

    def on_touch_down(self, touch):
        for w in self.parent.children:
            if str(type(w).__name__) == "SongLabel":
                if w.collides(touch.pos):
                    sid = w.id.split("_")[1]
                    self.root.set_current(sid)
                    self.root.player.change(sid)
                    return True
